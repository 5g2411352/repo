# ReadMe

# ReadMe

This is a wrapper / toplevel project for managing development of URLLC / SIB9 support in OpenAirInterface5G.
The framework uses the Google "repo" tool to manage multi-repository project (oAI gNodeB, Open5GS core etc. etc.)
in a consistent way.

The `repo` tool uses manifest files ( `default.xml`, `<manifest>.xml` ) to record the upstream repositories for
the components and specific version / revision / tags that need to be checked out.

`default.xml` is used when the manifest file is not specified on the `repo` command line with the `-m <manifest>.xml`
parameter.

## Usage

```

mkdir project-top

cd project-top

```

### Default manifest file

Initialize the `repo` tool metadata.

```

repo init -u  git@gitlab.com:5g2411352/repo.git

```

Now fetch the sub-projects from the upstream repositories.

```

repo sync -j 8 -c

```

`-c` above restricts the cloning to the current branch only, making it faster and `-j <n>` specifies the number of parallel threads to be used.

### Use selected manifest file

For using a specific manifest file, use the `repo init` command as follows:

```

repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab.xml

```

Or

```

repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-chronos.xml

```

The above uses the `oai-noS1-chronos.xml` from the default branch, e.g. `master`. If we need to use manifest
from another branch e.g., `main`, use the following:

```
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab.xml -b main
```

## Notes

### ASN1C installation

1. The 'asn1c' installed by `apt` may be too old. To install the latest version, follow the instructions
   below.

   ```
   pushd ./code/gNodeB/cmake_targets/
   ./build_oai -I --install-optional-packages

   ```

   This will usually install `asn1c` at `/usr/local/bin/asn1c` or `/opt/asn1c/bin/asn1c`. If the system has multiple `asn1c` exists, it may be needed to add the correct path to the `asn1c` executable to the `PATH` environment variable.

   ## Tracing & Debugging

   ### Wireshark / pcap

   #### Reference


   * [How to use Wireshark to trace LTE/NR protocols](https://gitlab.eurecom.fr/oai/openairinterface5g/-/blob/develop/openair2/UTIL/OPT/README.txt)

   #### Steps

   - How to configure wireshark for dissecting LTE/NR protocols:

     start the wireshark as a sudoers

     - goto analyze->enabled prototols
     - enable mac_xxx_udp and rlc_xxx_udp (xxx is lte or nr)
     - goto edit/preferences and expand Protocols
     - select UDP and check "try heuristic sub-dissectors first"
     - select MAC-LTE (or MAC-NR), and check all the options (checkboxes), and set the "which layer info to show in info column" to "MAC info"
     - select RLC-LTE (or NR), and check all the options except the "May see RLC headers only", and
       set the "call PDCP dissector for DRB PDUs" to "12-bit SN". Optionally you may select the sequence analysis for RLC AM/UM.
     - select PDCP-LTE (or NR)
   - How to use

     - start eNB or UE with option `--opt.type wireshark`
     - ``--opt`` options are

       `--opt.type` none/wireshark/pcap

       `--opt.ip` 127.0.0.1 to specify the output IP address (default: 127.0.0.1)
       output port is always: 9999 (to change it, change constant: PACKET_MAC_LTE_DEFAULT_UDP_PORT in OAI code)

       `--opt.path` file_name to specify the file name (pcap)
     - capture on local interface "lo"
     - filter out the ICMP/DNS/TCP messages (e.g. "!icmp && !dns && !tcp")

### Dependencies

#### DPDK

2024.wk01 requires dpdk-20.11.9.tar.xz instead of dpdk-20.05.tar.xz

1. Download DPDK version 20.11.9

   ```bash
   wget http://fast.dpdk.org/rel/dpdk-20.11.9.tar.xz
   ```
2. DPDK Compilation

   ```bash
   tar -xvf dpdk-20.11.9.tar.xz
   cd dpdk-stable-20.11.9
   meson build
   cd build
   sudo ninja
   sudo ninja install

   make install T=x86_64-native-linuxapp-gcc
   ```

## Lists

| No | Manifest Name                             | Remark                                                |
| -- | ----------------------------------------- | ----------------------------------------------------- |
| 1  | oai-noS1-chronos.xml                      |                                                       |
| 2  | oai-noS1-gitlab-wk45_jk.xml               |                                                       |
| 3  | oai-noS1-gitlab-track-upstream.xml        |                                                       |
| 4  | oai-noS1-gitlab-track-upstrr-memfixes.xml | Working configuration for fixing memory errors of OAI |

Initialisation commands for selected  configurations.

```bash
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-chronos.xml
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab-wk45_jk.xml
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab-2024w01.xml
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab-track-upstream.xml
repo init -u git@gitlab.com:5g2411352/repo.git -m oai-noS1-gitlab-track-upstrr-memfixes.xml
```

## Reference

For more info on `repo` and it's usage follow the following links.

1. [Using Repo and Git](https://wladimir-tm4pda.github.io/source/git-repo.html)
2. [Repo command reference](https://source.android.com/docs/setup/create/repo)
3. [Repo Flow](https://github.com/GatorQue/git-repo-flow) - version of `repo` with `git flow` support and also with `repo push` command added to use without gerrit server.
4. [Repo Cheatsheet](https://docs.sel4.systems/projects/buildsystem/repo-cheatsheet.html)
5. [repo - tool for getting Android source](https://manpages.ubuntu.com/manpages/xenial/man1/repo.1.html)
6. [git-flow cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/) - info on git-flow
7. [USRP Hardware Driver and USRP Manual](https://uhd.readthedocs.io/en/latest/page_devices.html)
8. [Verifying the Operation of the USRP Using UHD and GNU Radio](https://kb.ettus.com/Verifying_the_Operation_of_the_USRP_Using_UHD_and_GNU_Radio)

Also refer to wiki pages for additional usage notes.
